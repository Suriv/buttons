// Loading Begin  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
(function() {
    // polyfill remove
    if (!Element.prototype.remove) {
        Element.prototype.remove = function remove() {
            if (this.parentNode) {
                this.parentNode.removeChild(this);
            }
        };
    }
    // * polyfill remove


    // from http://www.sberry.me/articles/javascript-event-throttling-debouncing
    function throttle(fn, delay) {
        var allowSample = true;

        return function(e) {
            if (allowSample) {
                allowSample = false;
                setTimeout(function() { allowSample = true; }, delay);
                fn(e);
            }
        };
    }

    // check support for css properties
    function supportCSS(prop) {
        var yes = false; // по умолчанию ставим ложь
        var prop1 = prop.replace(prop[0], prop[0].toUpperCase());
        if ('Moz' + prop1 in document.body.style) {
            yes = true; // если FF поддерживает, то правда
        }
        if ('webkit' + prop1 in document.body.style) {
            yes = true; // если Webkit поддерживает, то правда
        }
        if ('ms' + prop1 in document.body.style) {
            yes = true; // если IE поддерживает, то правда
        }
        if (prop in document.body.style) {
            yes = true; // если поддерживает по умолчанию, то правда
        }
        return yes; // возращаем ответ
    }
    if (!(supportCSS('pointerEvents'))) {
        document.documentElement.classList.add('no-pointer_events');
    }
    if (!(supportCSS('transform'))) {
        document.documentElement.classList.add('no-transform');
    }
    // /* check support for css properties

    // check support for placeholder
    if (!("placeholder" in document.createElement('input'))) {
        var _root = document.documentElement;
        var _head = document.head;
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.async = true;
        script.src = "https://cdnjs.cloudflare.com/ajax/libs/placeholders/4.0.1/placeholders.min.js";
        _head.appendChild(script);
        _root.classList.add('no-placeholder');
    }
    // /* check support for placeholder

    // Ширина скролбара
    ;(function() {
        const scrollDiv = document.createElement('div'),
            body = document.body;
        let scrollW = 0;
        scrollDiv.style = 'position: absolute; top: -9999px; width: 50px; height: 50px;    overflow: scroll;';
        body.append(scrollDiv);
        scrollW = (scrollDiv.offsetWidth - scrollDiv.clientWidth) * 0.01;
        scrollDiv.remove(scrollDiv);
        document.documentElement.style.setProperty('--scroll-w', `${scrollW}px`);
    })();
    // * ширина скролбара

    // Высота для мобильного окна для фильтрации
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
    window.addEventListener('resize', () => {
      let vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
    });
    // * высота для мобильного окна для фильтрации

    // обертка для table in editor
    (function(){
        var tables = document.querySelectorAll(".editor table");
        [].forEach.call(tables, function(table) {
            var div = document.createElement("div");
            div.className = "table__cont";
            table.parentNode.insertBefore(div, table);
            div.appendChild(table);
        });
    })(window, document);
    // * обертка для table

    (function() {
        // Checkbox dop show/hide
        const checkboxDop = function (el){
            const container = el.closest('.g-checkbox-cont');
            let boxActive = container.querySelector('.g-checkbox.is-active');
            if (boxActive) {
                boxActive.classList.remove('is-active');
            }
            if( el.checked ){
                el.closest('.g-checkbox').classList.add('is-active');
                boxActive = el.closest('.g-checkbox');
            }
        }
        window.checkboxDop = checkboxDop;
        // * checkbox dop show/hide

        // Password input show/hide
        var passBtn = document.querySelectorAll(".js-input-pass__btn");
        if(passBtn.length > 0){
            [].forEach.call(passBtn, function(item){
                const inputPass = item.parentNode.querySelector('input');
                item.addEventListener('click', function(event){
                    if(inputPass.type == "password"){
                        inputPass.type = "text";
                    } else { inputPass.type = "password"; }
                })
            });
        }
        // * password input show/hide

        // function getImgSize(file) {
        //     var newImg = new Image();
        //     var reader = new FileReader();
        //         reader.readAsDataURL(file[0], 'UTF-8');
        //         reader.onload = function (event) {
        //             var result = event.target.result;
        //             newImg.src = result;
        //             var height = newImg.height;
        //             var width = newImg.width;
        //             newImg.onload = function() {
        //                 var height = newImg.height;
        //                 var width = newImg.width;
        //                 console.log('Ширина:' + width + ' Высота:' + height);
        //             }
        //         };
        // }
        // upload button
        const inputFile = document.querySelector(".js-input_file");
        if (inputFile) {

            inputFile.onchange = function(){
                let _this = this,
                    _file = _this.files,
                    _cont = _this.closest(".g-upload"),
                    _txtSize = _cont.querySelector(".js-input_file-size");
                    _del = _cont.querySelector(".js-input_file-del");
                if (_file != undefined && _file.length > 0){
                    let size = ((_file[0].size/(1024*1024)).toFixed(3));
                    if (size > 0.09) {
                        _txtSize.innerHTML = ( size + " Мб");
                    } else _txtSize.innerHTML = ( (_file[0].size/(1024)).toFixed(3) + " Кб");
                    // getImgSize(_file);
                }
                if( _this.value != "" ){
                    _cont.classList.add("is-file");
                } else _cont.classList.remove("is-file");

                _del.addEventListener('click', () => {
                    _this.value = '';
                    _file = '';
                    _cont.classList.remove("is-file");
                });
            };
        }
        // * upload button
    })(window, document);

    const innerLinks= document.querySelectorAll('.js-scroll');
    [].forEach.call(innerLinks, function(link){
        link.addEventListener("click", function(e){
            e.preventDefault();
            let _target = link.getAttribute("data-target");
            let _flagTotop = link.classList.contains("is-up");
            console.log(_target);
            if (_flagTotop) {
                document.body.scrollIntoView({
                    behavior: 'smooth'
                });
            } else {
                document.querySelector(_target).scrollIntoView({
                    behavior: 'smooth'
                });
            }
        });
    });

    const material = document.querySelector("#material");
    if (material) {
        const materialOffsetTop = material.offsetTop - 30;
        function scrollY() { return window.pageYOffset || docElem.scrollTop; }
        if(window.scrollY > material) {
            document.querySelector(".js-scroll").classList.add('is-up');
        }
        window.onscroll = function() {
            if(window.scrollY > material) {
                document.querySelector(".js-scroll").classList.add('is-up');
            }
            if(window.scrollY < material) {
                document.querySelector(".js-scroll").classList.remove('is-up');
            }
        };
    }

})(window, document);
// Loading End >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
